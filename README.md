# Simple Podman Infrastructre Experiments

## Overview

Experiments with setting up and running containers with podman and systemctl to 
start/run containers for various services.  Hoping to use rootless podman also. 

__NOTE:__ Things are not that easy due to being on an Apple Silicon Mac as Arch support on 
ARM isn't as easy as on x86_64

## Quickstart 




## Setup Instructions

### VMWare 

* Install VMWare Fusion on Mac (or workstation on Linux/Windows)


### Lima

* Follow the [lima installation docs](./docs/lima-installation)



## Notes

These are not steps to run, just things I tried



## References

### Arch Linux

* [Downloads x86_64](https://archlinux.org/download/)
* [Official x86_64 images](https://gitlab.archlinux.org/archlinux/arch-boxes/-/packages)


### Docker

* [nginx image](https://hub.docker.com/_/nginx)


### Lima

* [Lima homebrew formula](https://formulae.brew.sh/formula/lima)
* [Colima homebrew formula](https://formulae.brew.sh/formula/colima)
* [Colima github](https://github.com/abiosoft/colima)
* [Lima repo and information](https://github.com/lima-vm/lima)
* [Lima homepage](https://lima-vm.io/)
* [Colima nerdctl](https://github.com/containerd/nerdctl) for interacting with containerd
* [Lima Ubuntu LTS config](https://github.com/lima-vm/lima/blob/master/examples/ubuntu-lts.yaml)


### VMWare

* [Unofficial Ubuntu VMWare Fusion Guide](https://communities.vmware.com/t5/VMware-Fusion-Documents/The-Unofficial-Fusion-13-for-Apple-Silicon-Companion-Guide/ta-p/2939907)


### Linux

* [Ubuntu Linux 6.2 Kernel install](https://www.omgubuntu.co.uk/2023/08/ubuntu-22-04-linux-kernel-6-2)
* [IP Masquerading HOWTO](https://tldp.org/HOWTO/html_single/IP-Masquerade-HOWTO/)
* [Changing Ubuntu Timezone with timedatectl](https://www.hostinger.co.uk/tutorials/how-to-change-timezone-in-ubuntu/#:~:text=To%20do%20so%2C%20open%20Terminal,is%20using%20the%20timedatectl%20command.)
* [Cloud Init](https://docs.aws.amazon.com/linux/al2023/ug/cloud-init.html)


