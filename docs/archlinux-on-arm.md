# Arch Linux on ARM

## Overview

## Setup

### Arch ARM on VMWare

* Downloaded the Arch ISO from [IComplainInComments](https://github.com/IComplainInComments/archiso/releases/download/v3.0/archlinuxarm-baseline-2023.05.05-aarch64.iso)
* Started VMWare Fusion and selected "Create VM From Disc Image"
* When the VM boots select the on-screen default to install
* You will see lots of console logs
* Enter "root" and press return to login
* Follow instructions to install
* Also, before running `pacstrap` run the following

```
pacman-key --init
pacmab-key --populate archlinuxarm
```

* Install the system with `pacstrap` as per the instructions
* Install the grub UEFI loader

```
grub-install --target=arm64-efi --bootloader-id=grub_uefi --efi-directory=/boot/EFI --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

* Exit chroot shell
* Unmount filesystems
* Reboot
* Need to enable networking

```
systemctl enable systemd-networkd
systemctl enable systemd-resolved

systemctl start systemd-networkd
systemctl start systemd-resolved
```

* 


## Notes

### Booting at grub prompt

I got the install wrong in the first place and the system wouldn't boot properly.  The first issue was
no EFI boot.  The `grub-install` fixed that.  That then got to the `grub>` prompt but wouldn't go further.
Ran the following commands

```
set root=(hd0,3)
linux (hd0,1)/Image root=/dev/nvme0n1p3
initrd (hd0,1/initramfs-linux.img
boot
```

### Booting from CDROM

Under VMWare fusion you can go to the `Startup Image` option under `Settings`

### GPG 

Testing retrival of keys

```
gpg --debug-level guru --verbose --keyserver hkps://keyserver.ubuntu.com:443 --search-keys builder@archlinuxarm.org
```

This doesn't work on Arch and needs fixing with the following commands

```
rm /etc/resolv.conf
ln -s /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

### Marginal Trust for Arch ARM Linux builder key

This kept being a problem no matter how many times I cleared out the `/etc/pacman.d/gnupg` directory 
and ran `pacman-key --init` or tried to follow any number of similar steps.

The only solution was to using `pacman-key --edit builder@archlinuxarm.org` and mark the key as trusted and
then sign it with the local key (needed to have run `pacman-key --init` to generate that).  Seemed to work 
after that and packages could be installed.



## References

### Arch Linux

* [Arch Installation Guide](https://wiki.archlinux.org/title/Installation_guide)
* [Fixing missing key](https://superuser.com/questions/1675153/error-key-xxx-xxx-could-not-be-looked-up-remotely-on-archlinux)
* [Keyring not writable](https://bbs.archlinux.org/viewtopic.php?id=291644)
* [Install and boot Arch Linux on UEFI system](https://www.tecmint.com/arch-linux-installation-and-configuration-guide/)
* [Arch boot process](https://wiki.archlinux.org/title/Arch_boot_process#)
* [UEFI Interface](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface)
* [Systemd Networkd](https://wiki.archlinux.org/title/Systemd-networkd)
* [Systemd Resolved](https://wiki.archlinux.org/title/Systemd-resolved)
* [GPG Key retrieval failing](https://unix.stackexchange.com/questions/658389/are-gpg-keyservers-always-unreliable)
* [Arch Linux ARM key could not be looked up remotely](https://superuser.com/questions/1675153/error-key-xxx-xxx-could-not-be-looked-up-remotely-on-archlinux)
* [Arch Linux ARM keys](https://archlinuxarm.org/about/package-signing)
* [Arch Linux ARM key on Ubuntu key server](http://keyserver.ubuntu.com/pks/lookup?search=builder%40archlinuxarm.org&fingerprint=on&op=index)
* [Package is marginal trust](https://bbs.archlinux.org/viewtopic.php?id=267364)



### Arch ARM ISO

* [IComplainInComments ISO releases](https://github.com/IComplainInComments/archiso/releases)
* [IComplainInComments forked archiso repo](https://github.com/IComplainInComments/archiso)
* [Version 3.0 tag](https://github.com/IComplainInComments/archiso/tree/v3.0)


### Grub

* [Grub Arch Docs](https://wiki.archlinux.org/title/GRUB)
* [How to boot from Grub Shell](https://superuser.com/questions/1237684/how-to-boot-from-grub-shell)
* [Using the shell to boot](https://wiki.archlinux.org/title/GRUB#Using_the_command_shell_environment_to_boot_operating_systems)


### GNUPG

* [Server indicated failure](https://unix.stackexchange.com/questions/399027/gpg-keyserver-receive-failed-server-indicated-a-failure)
* [Arch GPG keys unreliable](https://bbs.archlinux.org/viewtopic.php?id=261267)
* [Unable to fetch keys from the command line](https://superuser.com/questions/1250681/why-am-i-unable-to-fetch-pgp-keys-from-commandline)
* [GPG Trust](https://security.stackexchange.com/questions/41208/what-is-the-exact-meaning-of-this-gpg-output-regarding-trust)



### Misc Linux

* [How to use timedatectl](https://www.howtogeek.com/782032/how-to-use-the-timedatectl-command-on-linux/)
* [List of timezones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

