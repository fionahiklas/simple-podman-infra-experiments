# Alpine Linux on ARM

## Overview

Installing podman under VMWare Fusion 


## Setup

* Download `aarch64` ISO image 
* Create new VM with that ISO as the CDROM image
* Boot
* Enter `root` at the login prompt (no password needed)
* Run this command

```
setup-alpine
```

* Answer the questions 
* Install completes, run `reboot`
* Login to the new OS using user and `su -` to get to root (or directly as root)
* Run the following to setup repos

```
setup-apkrepos -cf
```

* Start adding new packages using, for example `apk add <package>`



## References

### Alpine

* [Downloads](https://www.alpinelinux.org/downloads/)
* [Docs]()
* [Installation]()
* [UEFI on Alpine](https://wiki.alpinelinux.org/wiki/Alpine_and_UEFI)
* [Install Alpine with Desktop](https://linuxiac.com/how-to-install-alpine-linux/)
